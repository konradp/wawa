# wawa
url: https://konradp.gitlab.io/wawa

```
npm install
npm start
```
http://localhost:8080

# Notes
- drogo: Srodmiescie
- drogo: Zoliborz
- tanio: Wesola
- tanio: Ursus
- nowoczesne osiedla na Bernowie
- klimatyczne kamienice na Zoliborzu
- mieszkania przeksztalcone z hal fabryki na Ursusie
- 1.8mln osob w warszawie
- powierzchnia: 517,24 km2

dzielnice:
- Bemowo
- Białołęka
- Bielany
- Mokotów
- Ochota
- Praga-Południe
- Praga-Północ
- Rembertów
- Śródmieście
- Targówek
- Ursus
- Ursynów
- Wawer
- Wesoła
- Wilanów
- Włochy
- Wola
- Żoliborz

ranking:
- 2017: https://warszawa.stat.gov.pl/publikacje-i-foldery/warunki-zycia/ranking-dzielnic-warszawy-pod-wzgledem-atrakcyjnosci-warunkow-zycia,1,2.html
- 2013: https://warszawa.stat.gov.pl/publikacje-876/ranking-dzielnic-warszawy-pod-wzgledem-atrakcyjnosci-warunkow-zycia-1253/
- https://houseresponder.pl/blog/najpopularniejsze-dzielnice-mieszkalne-w-warszawie/
- https://rynekpierwotny.pl/wiadomosci-mieszkaniowe/gdzie-zamieszkac-w-warszawie/12001/

# Public transport

## Metro
url: https://www.metrolinemap.com/metro/warsaw/, view page source and get variables:
- metroStations
- metroLines

url: https://github.com/sdadas/warsaw-transport/blob/master/scripts/data/metro.json

url: https://github.com/sdadas/warsaw-transport, maybe here:
  - https://github.com/sdadas/warsaw-transport/blob/master/docs/includes/warsaw.json


# Other cities
- Sofia: 500 km2, population 1,281,040, density 2,600/km2
- Warszawa: 517 km2, population 1,864,056, density 3,601/km2
- Wroclaw: 292 km2, population 674,132, density 2,302/km2
- Poznan: 261 km2, population 540,146, density 2,063/km2
- Gdansk: 266 km2, population 486,492, density 1,800/km2
- Gdynia: 391 km2, population 257,000, density 1,820/km2

# References
- geojson files for district boundaries: https://github.com/andilabs/warszawa-dzielnice-geojson
- https://architektura.um.warszawa.pl/mapy
- https://www.citylines.co/data?city=warsaw#city
