let data = {};
let fetchedCount = 0;
let map = null;
let urls = {
  districts: './data/warszawa-dzielnice-geojson-master/warszawa-dzielnice.geojson',
  metroLines: './data/warszawa-metro/metro_lines.geojson',
  metroStations: './data/warszawa-metro/metro_stations.geojson',
  stats: './data/ranking.json',
};
let mapReferences = {
  info: null,
  metro: [],
  attractions: [],
  stations: [],
};
let stats = null;


window.onload = function() {
  map = L.map('map');
  // Download all data
  for (let key in urls) {
    fetch(urls[key])
      .then(res => res.json())
      .then((json) => {
        data[key] = json;
        fetchedCount++;
        if (fetchedCount == Object.keys(urls).length) {
          // All data fetched
          onDataFetched();
        }
      })
  }
}


function createInfoDiv(name) {
  let div = _new('div');
  // Title
  let h = _new('h1');
  h.innerHTML = name;
  div.appendChild(h);
  // ranking
  let r = _new('span');
  r.innerHTML = `ranking: ${stats[name].ranking}`;
  div.appendChild(r);
  return div.innerHTML;
}


function _new(tag) {
  return document.createElement(tag);
}


function heatMapColorforValue(value){
  var r = (1.0 - value) * 255;
  var g = value * 255
  return "rgb(" + r + ", " + g + ", 50)";
}


function onEachDistrict(feature, layer) {
  let name = layer.feature.properties.name;
  if (name == 'Warszawa') return;
  let ranking = stats[name].ranking;
  layer.setStyle({
    weight: 2,
    fillColor: heatMapColorforValue(1-ranking/18),
    color: 'black',
    dashArray: '',
    fillOpacity: 0.4
  });
  layer.on({
      mouseover: districtMouseOn,
      mouseout: districtMouseOff,
  });
}


function districtMouseOn(e) {
  // Highlight a district on hover
  const layer = e.target;
  let name = layer.feature.properties.name;
  let ranking = stats[name].ranking;

  layer.setStyle({
    weight: 3,
    color: 'black', // outline
    fillColor: heatMapColorforValue(1-ranking/18),
    dashArray: '',
    fillOpacity: 0.0
  });
  layer.bringToFront();
  let info = mapReferences.info;
  info.update(layer.feature.properties);

  // Rebring items to front
  for (i of mapReferences.metro) { i.bringToFront(); }
  for (i of mapReferences.stations) { i.bringToFront(); }
  for (i of mapReferences.attractions) { i.bringToFront(); }
}


function districtMouseOff(e) {
    let layer = e.target;
    let name = e.target.feature.properties.name;
    let ranking = stats[name].ranking;
    layer.setStyle({
      color: 'black',
      fillColor: heatMapColorforValue(1-ranking/18),
      fillOpacity: 0.4
    })
    let info = mapReferences.info;
    info.update();
}


function onDataFetched() {
  // When all data has been fetched
  stats = data.stats;
  drawDistricts();
  drawMetroLines();
  drawMetroStations();
  drawAttractions();
}


function drawDistricts() {
  let geoJSON = data.districts;
  L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      minZoom: 12,
      boundary: geoJSON,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
  }).addTo(map);
  var districtLayer = L.geoJSON(geoJSON);

  // Prevent from moving outside the bounds
  map.fitBounds(districtLayer.getBounds());
  var geojsonBounds = districtLayer.getBounds();
  map.setMaxBounds(geojsonBounds);

  // Add district borders
  L.geoJson(geoJSON, {
      onEachFeature: onEachDistrict
  }).addTo(map);

  // Info box
  var info = L.control();
  info.onAdd = function(map) {
    this._div = L.DomUtil.create('div', 'info'); // class=info
    this.update();
    return this._div;
  };
  info.update = function(props) {
    if (props) {
      this._div.innerHTML = createInfoDiv(props.name);
    } else {
      this._div.innerHTML = 'Hover over a district';
    }
  };
  info.addTo(map);
  mapReferences.info = info;
}


function drawMetroLines() {
  let geoJSON = data.metroLines;
  for (feature of geoJSON.features) {
    var polyline = L.polyline(
      feature.geometry.coordinates.map(coord => [coord[1], coord[0]]), {
      color: 'black',
      weight: 5,
    }).addTo(map);
    if (feature.properties.lineNameShort == 'M1') {
      polyline.setStyle({ color: 'blue' });
      map.fitBounds(polyline.getBounds());
    } else {
      polyline.setStyle({ color: 'red' });
    }
    mapReferences.metro.push(polyline);
  }
}


function drawMetroStations() {
  let geoJSON = data.metroStations;
  for (feature of geoJSON.features) {
    let circle = L.circle(
      [ feature.geometry.coordinates[1], feature.geometry.coordinates[0] ],
      {
        color: 'black',
        fillColor: 'black',
        fillOpacity: 1,
        radius: 80,
      }
    ).addTo(map);
    mapReferences.stations.push(circle);
    circle.bindPopup(feature.properties.stationName);
    circle.bringToFront();
  }
}


function drawAttractions() {
  // Draw dots for all attractions
  let stats = data.stats;
  for (district of Object.keys(stats)) {
    if ('attractions' in stats[district]) {
      for (attraction of stats[district].attractions) {
        drawAttraction(attraction.name, attraction.coords);
      }
    }
  }
}


function drawAttraction(name, coords) {
  // Draw a dot on an attraction
  let circle = L.circle(coords, {
      color: 'red',
      fillColor: '#f03',
      fillOpacity: 0,
      radius: 80,
      weight: 5,
  }).addTo(map);
  mapReferences.attractions.push(circle);
  circle.bindPopup(name);
  circle.bringToFront();
}
